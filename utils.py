import os
import classFile as cf
import glob
import csv
import pandas as pd



def searchForRegistry(workingPath=os.getcwd(),fileToSearch="databasePazienti.txt"):

    filePath = None 

    for root,dirs,files in os.walk(workingPath):
        if fileToSearch in files:
            filePath = os.path.join(root,fileToSearch)

    return filePath

def loadValues(fileName):

    vec = []

    with open(fileName) as fp:
        line = fp.readline()
        cnt = 1
        while line:
            #print("I've read this line {}: {}".format(cnt,line.strip()))
            p1 = cf.Person()
            splitedLine = line.split(",")
            p1.clinicCode = splitedLine[0]
            p1.id = splitedLine[1]
            p1.name = splitedLine[2]
            p1.sex = splitedLine[3]
            p1.category = splitedLine[4]
            p1.age = splitedLine[5]
            p1.date = splitedLine[6]
            p1.workCat = splitedLine[7]
            p1.instruction = splitedLine[8]
            p1.instruction = p1.instruction[:-1] #to delete special charachter
            vec.append(p1)
            line = fp.readline()
            cnt += 1
            
    return vec

    
def getTaskFile(folder,prefix="T*"):

        fileList = []
        
        #enter to working folder
        os.chdir(folder)
        
        for file in glob.glob(prefix):
                file = os.path.join(folder,file)
                fileList.append(file)
        
        print("I've found these files \n:",fileList)

        #back to main folder
        os.chdir("..")

        return fileList


def buildARFF(folder):

        fileList = getTaskFile(folder, '*.csv')

        #print("\nFile csv trovati: ", fileList)
        
        printProgressBar(0, len(fileList), prefix='Progress ARFF', suffix='Complete ARFF', length=50)
        i=0
        for file in fileList:
                fromCsv2Arff(file)
                printProgressBar(i+1, len(fileList), prefix='Progress ARFF', suffix='Complete ARFF', length=50)
                i += 1


        return


def fromCsv2Arff(file):

        features = []
        headers = None
        with open(file)as csvFile:
                data = csv.reader(csvFile)
                line_count = 0
                for row in data:
                        if line_count == 0:
                                headers = row[0].split(';')
                                #print("\nheader:\n",headers)
                                line_count += 1
                        else:
                                #print("\nrowPrima:\n",row[0])
                                features.append(row[0].replace(';',','))
                                #print("\nrowDopo:\n",row[0].replace(';',','))
                                line_count += 1

        folder = os.path.dirname(file)
        fileName = os.path.splitext(os.path.basename(file))[0] + '.arff'
        fileName = os.path.join(folder, 'arff',fileName)

        if not os.path.exists(os.path.dirname( fileName )):
                os.makedirs(os.path.dirname(fileName))
        
        if os.path.exists(fileName):
                os.remove(fileName)
                
        f = open(fileName,'w+') #open file in write mode
        f.write('@relation fileArff/trial1\n')
                
        for attr in headers:
                lineWrite = None
                if(attr == 'Sex' ):
                        line2Write = '@attribute ' + attr + ' {Mashile, Femminile}\n'
                elif attr == 'Work':
                        line2Write = '@attribute ' + attr + ' {Intellettuale, Manuale}\n'
                elif( attr == 'Label'):
                        line2Write = '@attribute Class {Malato, Sano}\n'
                else:
                        line2Write = '@attribute ' + attr + ' Numeric\n'
                f.write(line2Write)
                
        f.write('@data\n')
        for featureLine in features:
                f.write(featureLine+'\n')

        return


# Print iterations progress
def printProgressBar (iteration, total, prefix = '', suffix = '', decimals = 1, length = 100, fill = '█'):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + '-' * (length - filledLength)
    print('\r%s |%s| %s%% %s' % (prefix, bar, percent, suffix), end = '\r')
    # Print New Line on Complete
    if iteration == total: 
        print()