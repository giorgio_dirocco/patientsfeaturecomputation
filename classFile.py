

class Person:
    def __init__(self,name=None, age=None, instruction=None, workCat=None, sex=None, date=None, clinicCode=None, category=None, id=None):
        self.name = name
        self.age = age
        self.instruction = instruction
        self.sex = sex
        self.date = date
        self.clinicCode = clinicCode
        self.category = category
        self.id = id
        self.workCat = workCat