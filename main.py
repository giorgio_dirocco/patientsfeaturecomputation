import sys
import os.path
import utils
from argparse import ArgumentParser
import compt


def main():

    parser = ArgumentParser()
    parser.add_argument("-w", "--workingPath", default="")
    
    #collection of line arguments
    args = parser.parse_args()
    workingPath = args.workingPath

    #check if the working path exits
    if(not os.path.exists(workingPath)):
        workingPath = os.path.join(os.getcwd(),"data")
        print("Invalid input path, Set current directory as woriking path: ", workingPath)

    #search in the working path the file for registry information
    registryFile = utils.searchForRegistry(workingPath)

    if(registryFile is None):
        print("Registry File NOT FOUND!")
        return -1

    #load information from registry file
    registryVec = utils.loadValues(registryFile)

    if( len(registryVec) != 0):
        print("I've read {} patients!".format(len(registryVec)))
    else:
        print("No patient found in folder")

    #get list of TASK file in workingDirecory
    taskFileList = utils.getTaskFile(workingPath)

    if(len(taskFileList) == 0):
        print("\n No task file FOUNDED")
        return 0

    utils.printProgressBar(0, len(taskFileList), prefix='Progress CSV', suffix='Complete CSV', length=50)
    #extract ON-AIR & ON-PAPER features
    i = 0
    for file in taskFileList:
        copyElement = compt.onAir(file, registryVec)
        compt.onPaper(file, registryVec, copyElement)
        compt.onAirOnPaper(file, registryVec)

        utils.printProgressBar(i+1, len(taskFileList), prefix='Progress CSV', suffix='Complete CSV', length=50)
        i += 1

    #automatic build of ARRF file
    utils.buildARFF(os.path.join(workingPath,'outputData'))

       
    return 0


if __name__ == "__main__":
    main()