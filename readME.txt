I file presenti nella cartella permettono di elaborare le features estratte tramite movalyzer e in output vengono generati sia dei file CSV che dei file ARFF.
Il corretto utilizzo del programma prevede la presenza di una cartella "data" nella cartella dove vengono eseguiti gli script. 
Per eseguir il prgoramma basterà lanciare da riga di comando "python main.py".
In output nella cartella "data" verra generata una cartella "outputData" al cui interno saranno contenuti tutti i .CSV ottenuti. Inoltre nella stessa cartella
sarà presente una cartella "arff" che conterrà gli arff generati.
Per un corretto funzionamento degli script i file in input (.xlsx) dovranno essere nominati nella seguente maniera: T01, T02, ..., T33, T34. 
Inoltre è necessario che nella cartella "data" venga inserito anche il file di anagrafica "databasePazienti.txt".