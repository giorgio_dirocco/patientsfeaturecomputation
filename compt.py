import pandas as pd
import re
import numpy as np
import os

#recive an xlxs file and compute onAir features
def onAir(file, registryVec):

    #read file
    data = pd.read_excel(file)
    #data.as_matrix()
    data = pd.DataFrame(data)

    #extract only onAir traits
    data = data.loc[data['AveragePenPressure'] == 0]
    
    featuresVectors = pd.DataFrame(columns=getColumnsName('OnAir'))

    #compute patients by patiens
    for sampleID in data['Trial'].unique():
        featuresVectors.loc[len(featuresVectors)] = extractFeature( data.loc[data['Trial'] == sampleID ] )
    
    #print("FeaturesVector: ", featuresVectors)

    #merge with regisrty information
    completeOnAirFile = merge(featuresVectors, registryVec)

    #store file
    folder = os.path.dirname(file)
    outputFolder = os.path.join(folder,"outputData")
    if not os.path.exists(outputFolder):
        os.makedirs(outputFolder)
    
    outputFileName = os.path.join(outputFolder,'onAir_'+os.path.splitext(os.path.basename(file))[0]+'.csv')
    
    if os.path.exists(outputFileName):
        os.remove(outputFileName)

    #fill NaN with 0
    completeOnAirFile.fillna(0, inplace=True)
    copyElement = completeOnAirFile

    #print("Output\n",completeOnAirFile)
    completeOnAirFile=completeOnAirFile.to_csv(outputFileName,index=False,float_format='%.2f',sep=';')


    return copyElement


#recive an xlxs file and compute onPaper features
def onPaper(file, registryVec,copyElement):

    #read file
    data = pd.read_excel(file)
    #data.as_matrix()
    data = pd.DataFrame(data)

    #extract only onAir traits
    data = data.loc[data['AveragePenPressure'] != 0]

    featuresVectors = pd.DataFrame(columns=getColumnsName('OnPaper'))

    #compute patients by patiens
    for sampleID in data['Trial'].unique():
        featuresVectors.loc[len(featuresVectors)] = extractFeature( data.loc[data['Trial'] == sampleID ] )
    
    #merge with regisrty information
    completeOnPaperFile = merge(featuresVectors, registryVec)

    #store file
    folder = os.path.dirname(file)
    outputFolder = os.path.join(folder,"outputData")
    if not os.path.exists(outputFolder):
        os.makedirs(outputFolder)
    
    outputFileName = os.path.join(outputFolder,'onPaper_'+os.path.splitext(os.path.basename(file))[0]+'.csv')
    
    if os.path.exists(outputFileName):
        os.remove(outputFileName)

    #fill NaN with 0
    completeOnPaperFile = addMissingId(completeOnPaperFile,copyElement)
    completeOnPaperFile.fillna(0, inplace=True)

    #print("Output\n",completeOnPaperFile)
    completeOnPaperFile.to_csv(outputFileName,index=False,float_format='%.2f',sep=';')
    


    return 

def onAirOnPaper(file,registryVec):

    #read file
    data = pd.read_excel(file)
    #data.as_matrix()
    data = pd.DataFrame(data)

    featuresVectors = pd.DataFrame(columns=getColumnsName(''))

    #compute patients by patiens
    for sampleID in data['Trial'].unique():
        featuresVectors.loc[len(featuresVectors)] = extractFeature( data.loc[data['Trial'] == sampleID ] )
    
    #merge with regisrty information
    completeOnAirOnPaperFile = merge(featuresVectors, registryVec)

    #store file
    folder = os.path.dirname(file)
    outputFolder = os.path.join(folder,"outputData")
    if not os.path.exists(outputFolder):
        os.makedirs(outputFolder)
    
    outputFileName = os.path.join(outputFolder,'onAirOnPaper_'+os.path.splitext(os.path.basename(file))[0]+'.csv')
    
    if os.path.exists(outputFileName):
        os.remove(outputFileName)
    
     #fill NaN with 0
    completeOnAirOnPaperFile.fillna(0, inplace=True)
    #print("Output\n",completeOnAirOnPaperFile)

    completeOnAirOnPaperFile.to_csv(outputFileName,index=False,float_format='%.2f',sep=';')


    return

def extractFeature(values):
   
    row = []
    
    for column in values:
        if column == 'Trial' or column == 'Score' or column =='NumOfStrokes' :
            if column != 'NumOfStrokes':
                row.append( values[column].iloc[0] )
            else :
                row.append( values[column].count() )
        elif column == 'Duration':
            row.append( values[column].sum()  )
            row.append( values[column].min()  )
            row.append( values[column].max()  )
            row.append( values[column].mean() )
            row.append( values[column].var()  )
        elif re.search('PerTrial',column) or re.search('Trial',column) or column == 'RelativePenDownDuration' or column == 'RelativeDurationofPrimary' or column == 'RelativeSizeofPrimary' or column == 'FrequencyofSecondary' or column == 'Segment' or column == 'StartTime':
            continue
        else:
            row.append( values[column].min()  )
            row.append( values[column].max()  )
            row.append( values[column].mean() )
            row.append( values[column].var()  )

    #print("The row build is\n: ", len(row) )

    return row


def getColumnsName(suffix):

    columns = [
        'Id',
        'DurationTot', 'DurationMin', 'DurationMax', 'DurationMean','DurationVar',
        'StartVerticalPositionMin','StartVerticalPositionMax','StartVerticalPositionMean','StartVerticalPositionVar',
        'VerticalSizeMin','VerticalSizeMax','VerticalSizeMean','VerticalSizeVar',
        'PeakVerticalVelocityMin','PeakVerticalVelocityMax','PeakVerticalVelocityMean','PeakVerticalVelocityVar',
        'PeakVerticalAccelerationMin','PeakVerticalAccelerationMax','PeakVerticalAccelerationMean','PeakVerticalAccelerationVar',
        'StartHorizonatalPositionMin','StartHorizonatalPositionMax','StartHorizonatalPositionMean','StartHorizonatalPositionVar',
        'HorizontalSizeMin','HorizontalSizeMax','HorizontalSizeMean','HorizontalSizeVar',
        'StraightnessErrorMin','StraightnessErrorMax','StraightnessErrorMean','StraightnessErrorVar',
        'SlantMin','SlantMax','SlantMean','SlantVar',
        'LoopSurfaceMin','LoopSurfaceMax','LoopSurfaceMean','LoopSurfaceVar',
        'RelativeInitialSluntMin','RelativeInitialSluntMax','RelativeInitialSluntMean','RelativeInitialSluntVar',
        'RelativeTimeToPeakVerticalVelocityMin','RelativeTimeToPeakVerticalVelocityMax','RelativeTimeToPeakVerticalVelocityMean','RelativeTimeToPeakVerticalVelocityVar',
        'AbsoluteSizeMin','AbsoluteSizeMax','AbsoluteSizeMean','AbsoluteSizeVar',
        'AverageAbsoluteVelocityMin','AverageAbsoluteVelocityMax','AverageAbsoluteVelocityMean','AverageAbsoluteVelocityVar',
        'RoadlengthMin','RoadlengthMax','RoadlengthMean','RoadlengthVar',
        'AbsoluteyJerkMin','AbsoluteyJerkMax','AbsoluteyJerkMean','AbsoluteyJerkVar',
        'NormalizedyJerkMin','NormalizedyJerkMax','NormalizedyJerkMean','NormalizedyJerkVar',
        'AbsoluteJerkMin','AbsoluteJerkMax','AbsoluteJerkMean','AbsoluteJerkVar',
        'NormalizedJerkMin','NormalizedJerkMax','NormalizedJerkMean','NormalizedJerkVar',
        'Score',
        'NumberOfPeakAccelerationPointsMin','NumberOfPeakAccelerationPointsMax','NumberOfPeakAccelerationPointsMean','NumberOfPeakAccelerationPointsVar',
        'AveragePenPressureMin', 'AveragePenPressureMax', 'AveragePenPressureMean', 'AveragePenPressureVar',
        'NumOfStrokes'
    ]

    
    return columns

def merge(featuresVectors, registryVec):
    #print("Features vector:\n ", featuresVectors)
    appFile = None
    completeFile = None

    for patient in registryVec:
        if not (featuresVectors.loc[featuresVectors['Id'] == float(patient.id)]).empty:
            appDf = None
            appFile = None
            appDf = pd.DataFrame(columns=['Sex', 'Age', 'Work','Instruction','Label'])
            appDf = appDf.append({'Sex': patient.sex,'Age':patient.age, 'Work':patient.workCat,'Instruction':patient.instruction,'Label':patient.category}, ignore_index=True)
            appFile = pd.DataFrame( np.hstack([ featuresVectors.loc[featuresVectors['Id'] == float(patient.id)], appDf ]), columns=[ np.concatenate([ featuresVectors.columns, appDf.columns]) ] )
            completeFile = pd.concat( [completeFile, appFile] )
    
    #print("Complete file:\n",completeFile)
    

    return completeFile


def addMissingId(data, copyData):

    data.reset_index(inplace=True, drop=True)
    copyData.reset_index(inplace=True, drop=True)

    for id in copyData['Id'].values:
        if id not in data['Id'].values:
            data=data.append( copyData.loc[ id-1 ] )
    
    data = data.sort_index(level=1, sort_remaining=False)
    #print("Data:\n",data)
   
    
    return data